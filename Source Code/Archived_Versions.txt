Note: Repeat files are documented as they function in Lab0xFF; previous versions of these files are still available to view through the bitbucket repository linked for each lab.

List of archived/(removed from doxygen scanning) files:

For all labs:
- main.py
For Lab 2-5
- taskUser.py
For Lab 3-5
- motor.py
- taskMotor.py
For Lab 4-5
- ClosedLoop.py
- taskController.py
For Lab 5
- BNO055.py
- taskBNO055.py