'''!@file       hw0x02_page.py
    @brief      A page providing documentation and supporting material for ME 305 Hw0x02

    @page hw0x02 ME 305 Hw0x02
 
    @section Hw0x02 Hw0x02: Ball Balancing Model

    Below are the solutions to Homework 2, outlining a simplified model of a pivoting platform used to balance a ball

    @image html hw0x02_pg_1.jpg "HW0x02 Page 1" width=500

    @image html hw0x02_pg_2.jpg "HW0x02 Page 2" width=500

    @image html hw0x02_pg_3.jpg "HW0x02 Page 3" width=500
        
    @author Ally Lai
    
    @copyright no associated license

    @date February 13, 2022
'''