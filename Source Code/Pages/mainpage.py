'''!@file       mainpage.py
    @brief
    @details

    @mainpage ME 305 Documentation Main Page
 
    @section intro_sec ME 305 - Introduction to Mechatronics

    In this course, several introductory topics within Mechatronics are covered including high-level design of 
    mechatronic systems, creating time-efficient software solutions, and documentation of hardware and software design.

    Some of the basic tasks covered by this course are interfacing with a simple encoder, creating a user interface, Closed-Loop
    motor control of a DC motor, and collecting and displaying data.
    
    @subsection me305lab ME 305 Lab Assignments
    
    Documentation is categorized by page under the "Related Pages" tab for lab assignments completed as part of this course.

    @author Ally Lai
    
    *Lab assignments completed in collaboration with Jason Hu (jjhu@calpoly.edu)
   
    @copyright no associated license

    @date February 3, 2022
    
'''