'''!@file       lab0xFF_page.py
    @brief      A page providing documentation and supporting material for ME 305 Lab0xFF

    @page lab0xFF ME 305 Lab0xFF
 
    @section Lab0xFF Lab0xFF: Term Project - Ball-Balancing Platform
    
    @subsection Introduction:
        
    This project involves several different pieces of hardware which we worked with thoughout the quarter to balance a ball on top of a platform. The equipment used 
    in the balancing platform includes two PMDC motors, a BNO055 IMU chip, a resistive touchpanel, and an STM32 Nucleo microcontroller. The project was written in 
    Micropython to interface via serial port with the controller board and connected hardware.
    
    @subsection Files
      
    Three different types of files were used to create this project First are driver files which directly interact with hardware
    and are instantiated based on specific pins on the microcontroller board. Examples of driver files include the touchpanel driver and the motor driver. 
    Second are task files are functions which run as part of a generator to form a cooperative multitasking system. They do not interact with the hardware but instead interact with the 
    raw values read from methods in the driver classes. Examples of Task files include TaskTouchPanel, TaskMotor, Taskcontroller and Taskuser. 
    Lastly we have the main file, which runs each task in a specfic order based on the given frequency. In this main file, share objects are instatiated which faciliate communication between all the files. 
    
    @subsection controls Closed Loop Control System
    
    The image below shows a visual representation of the control system used to balance the ball of the platform:
        
    @image html lab0xFF_block_diagram.JPG "Block Diagram" width=1000
    
    The system employs two nested loops of closed loop feedback along with saturation limits and hardware-related corrections
    to output duty cycles for the two motors in a way that maneuvers the platform to balance a metal ball. (A video of the control
    system in action is located at the bottom of this page. The entire system was created for the x and y direction of motion separately
    in order to simplify the model. A more detailed description of the analysis behind this system is shown in the pages for HW0x02 and
    HW0x03. Below is a more detailed description of the components of the control system.

    1) Closed Loop Feedback Loops:
    
    The controller has two distinct control loops, an "outer loop" which responds to the location and velocity of the ball, (indicated by the touch panel),
    and an "inner loop" which reponds to the angle and angular velocity of the platform. Each loop contained both proportional and derivative gain and the output
    for the outer control loop was the desired platform angle and the output for the inner control loop was the duty cycles of the motors.
    
    *When the ball is not in contact, the platform is leveled by the same system excluding all of the aspects outside the IMU offset block and the gravity and static friction compensation. The controller
    gains Kp and Kd were also separately tuned and are listed in the next section regarding tuning.   
    
    2) IMU Offset

    In order to account for error in the absolute orientation reading from the IMU, a level was placed on the platform to move it to a level position and the euler
    angles from the IMU were printed continuously in order to see how much the IMU readings were offset from level. The level on the platform and correspinding terminal output
    are shown below.
                                                                                                        
    @image html lab0xFF_level_image.jpg "Bubble Level" width=400
    
    @image html lab0xFF_euler_angles.PNG "Euler Angle Output" width=500
    
    3) Gravity and Static Friction Compenstation
    
    In order to account for the duty cycle required to keep the platform balanced, (as the components of the platform are unevenly distributed), a gravity compensation is added to the output duty cycle
    based on the resting duty cycle when the platform is empty but balanced. A compensation of a couple percentage points is also added to the duty cycle outputs to help offset the static friction in the motors
    that has to be overcome to begin motion.
    
    4) Saturation Limits
    
    For both the reference angle output and duty cycle output of the control loops a saturation limit was set in order to provide more stability to the system and avoid cuasing issues with the hardware. For the
    angle of the platform the limits were +- 12 degrees and for the duty cycle the limits were +-45%.
        
    @subsection tuning Controller Tuning
       
    In order to find working values of proportional gain (Kp), derivative gain (Kd), and integral gain (Ki), we started with our gain values used to balance the board in the previous lab and slowly increased our
    out loop gains to create a working control loop. Throughout the tuning process, data and graphs were collected and plotted to gain insight on the inner workings of tuning. Data collected includes the X and Y position and 
    velocity and the duty cycle values. We did this for both alpha-beta filtered and unfiltered calculations. Images of position, velocity and duty cycle versus time are shown below.
    Multiple iterations of these graphs were made, and the values of Kp, Ki, Kd, alpha and beta were changed until the platform exhibited behavior that we felt was reasonable for this project. 
    At the submission of this project, the variable values are as follows:
    
                        Inner Loop    Outer Loop (x)   Outer Loop (x)   Inner Loop(No Ball)
                Kp:       7.565     |   0.185        |    0.200       |         6.0
                Kd:       0.325     |   0.155        |    0.140       |        0.3
                Ki:       0.000     |   0.000        |    0.000       |        4.0

    @subsection abfilter Alpha-Beta Filtering

    In order to decrease noise in the touch panel signal and estimate velocity, an alpha-beta was applied to the data and tuned along with the controller to find optimal
    values for alpha and beta. Below are the final values used and an example of the filtered vs. unfiltered data from the touch panel. This filtering is especially important
    for the control system because large changes in duty cycle to the motor caused more issues with belt-slipping and instability of the closed loop output in general.
          
                Alpha:             0.80
                Beta:              0.51
        
    @image html lab0xFF_unfiltered.PNG "Unfiltered X-Y Data" width = 750
    @image html lab0xFF_filtered.PNG "Filtered X-Y Data" width=750       
    
    @subsection std State Transition Diagram
    
    The image velow shows the State Transition Diagram for the program as a whole. The entirety of the program is controlled by 'taskUser.py' which specifies what will be done and when. All the files run at
    10000Hz. It is important not to have blocking code so all these states can run at the same time. When the user presses buttons, the file 'taskUser.py' sends out flags to other files to perform operations. 
    An example of this is when the user hits the F button, taskUser sends out a flag to taskTouchpanel to start alpha-beta filtering the positions and velocities of the ball. 
    
    @image html lab0xFF_task_diagram.JPG "Task Diagram" width=750
    
    The Task Diagram shown above shows how each shared variable is used to communicate between tasks. For example, when the user presses F for f_Flag and 'taskTouchpanel' sends the shares 'ball__coords' and 'ball_vel' 
    to the task controller file.   

    @subsection taskTiming Task Timing Check
    
    In order to check whether the TouchPanel task would run fast enough a test file "t.py" was written to estimate the average time
    required for one run of the "scanXYZ()" function of the Touch Panel Driver to see if the task would be able to call and recieve 
    touch panel data within one run of the generator. The result of running the test was that the scan function ran consistently within 
    an average of 500 us. Below is functional contents of the test file:
        
    @image html lab0xFF_test_script.PNG "Timing Test Script" width=750    

    @subsection userinteface User Interface
    
    Below is an image of the user interface printed upon startup of the program:   
        
                        +-------------------------------------+
                        | Welcome to the Ball Balancing       |
                        | Platforms User Interface            |
                        |-------------------------------------|
                        | To use the interface, press:        |
                        | "P" to print Euler angles from the  |
                        |         IMU                         |
                        | "V" to print angular velocity from  |
                        |         the IMU                     |
                        | "U" to print calibration status     |
                        | "C" to write calibration coeffs     |
                        |         to IMU                      |
                        | "Q" to calibrate touch panel        |
                        | "F" to filter touch panel readings  |
                        | "m" to enter duty cycle for motor 1 |
                        | "M" to enter duty cycle for motor 2 |
                        | "S" to stop data collection or test |
                        |      interface prematurely          |
                        | "H" to print this help menu         |
                        +-------------------------------------+
                        | Closed Loop Controller Functions:   |
                        |                                     |
                        | press "k" to enter new inner loop   |
                        |               gain values           |
                        | press "K" to enter new outer loop   |
                        |               gain values           |  
                        | press "W" to enable or disable      |
                        |           platform balancing        |
                        | press "J" to collect data for 15    |
                        |               seconds               |
                        +-------------------------------------+
    
    @subsection demonstrations Demonstation Videos
    
    Below are a collection of videos to help show the user interface and balancing capability of the program.The maximum time we were able to get the ball to balance was about 16 seconds. In order to do this, 
    the ball was placed in the middle of the platform with approximately zero velocity. This is analogous to having homogeneous initial conditions. (The X and Y coordinates and zero and the X any Y velocities are zero). 
    
    Demonstration of Ball Balancing
    
    \htmlonly
    <iframe width="560" height="315" src="https://www.youtube.com/embed/01COpgxKh2I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    \endhtmlonly
    
    User Interface Demonstration
    
    \htmlonly
    <iframe width="560" height="315" src="https://www.youtube.com/embed/JImQc8J4W1s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    \endhtmlonly
       
    Closed Loop Controller Demonstration
    
    \htmlonly
    <iframe width="560" height="315" src="https://www.youtube.com/embed/3G0gw5G3gmM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    \endhtmlonly
    
    Below is a link to the repository containg each of the source files for the project:
        
    Bitbucket Repository Link: https://bitbucket.org/alai09/alai09.bitbucket.io/src/main/Source%20Code/Lab0xFF/
        
    @author Ally Lai
   
    @author Jason Hu
    
    @copyright no associated license

    @date March 18, 2022
'''