'''!@file       lab0x03_page.py
    @brief      A page providing documentation and supporting material for ME 305 Lab0x03

    @page lab0x03 ME 305 Lab0x03
 
    @section Lab0x03 Lab0x03: DC Motor Control
    
    @subsection desc Description:
    
    Lab0x03 builds on the user interface from lab0x02 to allows interaction with a motor driver which can control up
    to two motors along with the same encoder. The interface prompts the user to enter different commands using specific key inputs. 
    The commands for the encoder are the same, (zeroing the relative position of the encoder, getting the current position or delta value,
    collecting position data for up to 30 seconds, and stopping data collection), except for the addition of being able to retrieve the 
    motor velocity. The commands for the motor(s) include setting the duty cycle for one of the two motors, going through a testing 
    interface whichruns the motor at the inputted motor speed and records its average velocity, and clearing potential fault conditions
    returned from the motor
    
    The following finite state diagrams outline the functionality of the user, motor, and encoder task files
    which interact with the user interface and motor and encoder inputs/outputs respectively.
    
    Below is a reference plot of 30 seconds of encoder position data recorded using this lab as well as a plot of average motor
    velocities vs. the inputted duty cycle for the motor. A task diagram and finite state diagrams for each task are also shown 
    to illustrate the structure of the code. A link to a repository containing the source code is also listed below. 
    Documentation of each file created for this lab can be found under the "Files" tab of this website.

    The following task diagram shows how data is shared between the user, encoder, and motor tasks:
        
    @image html lab0x03_TD.jpg "Lab0x03 Task Diagram" width=500
    
    The following finite state diagrams outline how the user, motor, and encoder task functions
    transition between states and their general behavior in each state.
    
    @image html lab0x03_userSD.jpg "User Task Finite State Diagram" width=500
    
    @image html lab0x03_encoderSD.jpg "Encoder Task Finite State Diagram" width=500
    
    @image html lab0x03_motorSD.jpg "Motor Task Finite State Diagram" width=500
    
    The following graph shows an example of what the output for 30 seconds of encoder data might look
    like. As visible in the graph, the position of the encoder is updates in discrete increments which 
    are visible to the eye at a sampling frequency of 100 Hz.
    
    @image html lab0x03_exEncData.jpg "Example Encoder Data" width=400
    
    The following graph shows the relationship between input duty cycle and average velocity of the motors
    used in this lab. The plot shows that the relationship between duty cycle and average velocity is fairly linear
    and symmetric. The expression for the line of best fit is pictured on the graph
    
    @image html lab0x03_duty_velocity_graph.jpg "Duty Cycle vs. Velocity Plot" width=400
    
    Bitbucket Repository Link: https://bitbucket.org/alai09/alai09.bitbucket.io/src/main/Source%20Code/Lab0x03/
        
    @author Ally Lai
   
    @author Jason Hu
    
    @copyright no associated license

    @date February 17, 2022
'''