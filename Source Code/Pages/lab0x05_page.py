'''!@file       lab0x05_page.py
    @brief      A page providing documentation and supporting material for ME 305 Lab0x04

    @page lab0x05 ME 305 Lab0x05
 
    @section Lab0x05 Lab0x05: I2C and Inertial Measurement Units
    
    @subsection desc Description:
        
    In this lab, a driver class and taks function for a BNO055 IMU was written in order to calibrate and read data for position and velocity
    of the device. The overall functionality of the lab was built off of lab0x04 and serves as intermediate step to the term project (lab0xFF)
    by integrating functionality of the IMU and tuning a control loop to set duty cycles to the two platform motors to keep it balanced.
    
    Bitbucket Repository Link: https://bitbucket.org/alai09/alai09.bitbucket.io/src/main/Source%20Code/Lab0x05/
        
    @author Ally Lai
   
    @author Jason Hu
    
    @copyright no associated license

    @date March 18, 2022
'''