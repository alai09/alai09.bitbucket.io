'''!@file       hw0x03_page.py
    @brief      A page providing documentation and supporting material for ME 305 HW0x03

    @page hw0x03 ME 305 Hw0x03
 
    @section Hw0x03 Hw0x03: Ball Balancer Simulation

    Below is a Matlab livescript used to run and plot the results of simulations for Homework 0x03:

    @htmlinclude HW0x03_Matlab.html
        
    @author Ally Lai
    
    @copyright no associated license

    @date February 27, 2022
'''