'''!@file       lab0x04_page.py
    @brief      A page providing documentation and supporting material for ME 305 Lab0x04

    @page lab0x04 ME 305 Lab0x04
 
    @section Lab0x04 Lab0x04: Closed Loop DC Motor Speed Control
    
    @subsection desc Description:
    
    Lab0x04 builds on the user interface from lab0x03 and add closed-loop control as a potential method to manipulate the speed of the motor.
    The interface prompts the user to enter different commands using specific key inputs. 
    The commands added to this lab since Lab 0x03 allow the user to pick a setpoint for velocity, a proportional gain,
    and run a 3-second-long step response based on these inputs from which relevant data is printed.
    
    This lab demonstrates the functionality of basic proportional gain which, as discussed further on this page, does not
    produce very accurate results for motor speed.

    The following task diagram shows how data is shared between the user, encoder, motor, controller tasks. The user task handles 
    user input and displaying the user interface while the encoder task interacts with encoder objects to produce return encoder data.
    The motor and controller classes both exchange information with the user class and between themselves in order to send signals to 
    the motor and calculate values for closed-loop motor control respectively.
        
    @image html lab0x04_task_diagram.jpg "Lab0x03 Task Diagram" width=500
    
    The following finite state diagrams outline how the user, motor, controller and encoder task functions
    transition between states and their general behavior in each state.
    
    @image html lab0x04_user_sd.jpg "User Task Finite State Diagram" width=750
    
    @image html lab0x04_encoder_sd.jpg "Encoder Task Finite State Diagram" width=600
    
    @image html lab0x04_motor_sd.jpg "Motor Task Finite State Diagram" width=400
    
    @image html lab0x04_controller_sd.jpg "Motor Task Finite State Diagram" width=500
    
    The following block diagram shows the structure of the basic proportional closed-loop
    control used to indicate an input duty cycle to the motor for this lab.
    
    @image html lab0x04_blockdiagram.jpg "Lab0x04 Closed Loop Control Block Diagram" width=600
    
    The following graphs show induvidually the reference velocity setpoint, measured velocity values, and input duty cycle
    over time. For each step response plotted, the setpoint for velocity was set to 80 rad/s
    
    At too large of a gain, such as 1.00, a motor fault is triggered causing the motor velocity to go down to 0 while the 
    input duty cycle settles at a value based on the measured velocity being 0. This behavior is shown in the following plot where
    it can be observed that the signal fluctuates initially then quickly evens out after the fault is triggered. The fault is triggered
    because the difference between the input duty cycle and 0, (the starting duty cycle), is too high.
    
    In this case, the settling time is 0 seconds because the velocity does not change significantly for zero but the steady-state error
    is highest at 80 rad/s because the motor is not moving.
    
    @image html lab0x04_kp_1.jpg "Step Response for Gain of 1.00" width=500
    
    At too small of a gain, such as 0.05, and 0.25, shown below, the settling time is small or zero but the steady state error is still
    relatively high because the actuation duty cycle indicated is fairly low. For a gain of 0.05 seconds there is  a 0.0 second settling time and 
    79-80 rad/s of steady-state error whereas for a gain of 0.25 there is a settling time of 0.0 seconds but around 59.2 rad/s of steady-state
    error.
    
    @image html lab0x04_kp_0_05.jpg "Step Response for Gain of 0.05" width=500
    
    @image html lab0x04_kp_0_25.jpg "Step Response for Gain of 0.25" width=500
    
    At a gain that is slightly higher than ideal but does not trigger a motor fault,
    the steadying time and steady-state error are higher than neccessary. For example, at a 
    gain of 0.60, the steady-state error is around 55.4 rad/s and the steadying time is 0.08 seconds.
    
    @image html lab0x04_kp_0_60.jpg "Step Response for Gain of 0.60" width=500
    
    Theoretically there is a gain value which minimizes the steady-state error. In this case,
    a gain of 0.50 led to the smallest steady-state error of the different gain values tested.
    The steady-state error in this case was around 45.0 rad/s and the settling time was 0.04 seconds.
    The ideal gain value could be further fine-tuned by testing higher and lower gain values to see what 
    ends up with the lowest steady-state error.
    
    @image html lab0x04_kp_0_5.jpg "Step Response for Gain of 0.50" width=500
    
    Bitbucket Repository Link: https://bitbucket.org/alai09/alai09.bitbucket.io/src/main/Source%20Code/Lab0x04/
        
    @author Ally Lai
   
    @author Jason Hu
    
    @copyright no associated license

    @date February 24, 2022
'''